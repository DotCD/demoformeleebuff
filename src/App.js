//react library
import React from 'react';
//css for the app
import './App.css';
//upload area component
import UploadArea from './components/UploadArea';


//this is the base component of the react app
class App extends React.Component {
  //this code is useless im 99% sure but I don't have time to test if it is lmfao
  state = {
    gameData: {
      name: "test",
      extension: ".slp"
    } 
  }
  //end useless

  render() {
    //this creates a header tag then adds the upload area component
    return (
      <div className="App">
        <h1>Parsing replays on the web</h1>
        <UploadArea />
      </div>
    );
  }
}

export default App;
