//importing specific parts of the react library
import React, { Fragment, useState } from 'react';
//axios is a library for creating http requests, it is much better than the standard JS implementation and makes life way better
import axios from 'axios';
//this is the slippi library which im pretty sure this file doesn't actually need but no time to test LOL
import { ConsoleConnection } from '@slippi/slippi-js';

const UploadArea = () => {
    //these are "react hooks" they're like variables you can put in the HTML so that the page updates automaticall when the variable changes
    //the square brackets contain a name for the variable then a name for the function to change the variable
    //the use state function sets their defualt value
    //they're also good because they are global within your component
    const [file, setFile] = useState('');
    const [fileName, setFileName] = useState('choose file');
    //the {} make this variable a object
    const [uploadedFile, setUploadedFile] = useState({});
    const [replayData, setReplayData] = useState('REPLAY DATA HERE');

    //change file data variables when the file button calls this function
    //the function will be called whenever the selected file changes
    //there is currently a bug where if you switch it to having no file selected it will crash I know how to fix the bug but not enough time lol
    const onChange = e => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };

    //this function is called when you click the submit/upload button
    const onSubmit = async e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);

        //a try catch statement jic the http request has a error
        try {
            //makking a HTTP post request using the axios library
            //sends the replay.slp file to the server at the server path /api/parse
            const res = await axios.post('http://localhost:8080/api/parse', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            });
            //log what the server reponds with (for debugging purposes)
            console.log(res);
            //pull two variables out of the HTTP responses data category
            const { fileName, filePath } = res.data;
            //set the data of the uploaded file
            //uploaded file hook not actually used in this project but would be very useful on a full implementation of melee buff
            setUploadedFile({fileName, filePath})
            //set the server path for getting the replay data
            const replayDest = 'http://localhost:8080' + filePath;
            //make a HTTP get request to get the replay data
            const replayRes = await axios.get(replayDest);
            //sets the replay data react hook to be what was returned
            //we use a react hook for this so the page can update to show the data as soon as the variable updates
            setReplayData(parseReplay(replayRes.data));
        } catch(err) {
            //catches errors and logs them
            if(err.response != null) {
                if(err.response.status === 500) {
                    console.log("server erorr")
                } else {
                    console.log(err.response.data.msg);
                    console.log(err);
                }
            } else {
                console.log(err);
            }
        }
    };

    function parseReplay(data) {
        //this will take take the JSON data returned by the HTTP request and parse it to a string that is much more human readable
        //this funciton could be much more efficient but I wrote it in like 3 seconds
        const player0Name = data.player0.name.netplay;
        const player1Name = data.player1.name.netplay;
        const player0hero = data.player0.hero;
        const player1hero = data.player1.hero;
        const dataString = player0Name + " Playing: " + player0hero + " " + player1Name + " Playing: " + player1hero;

        return dataString;
    }

    //this is the HTML for the component
    //it's not actually HTML it's JSX which has a few differences that are pretty minor but 99% is the same 
    //the fragment tag makes it so that when React inserts this into whatever component it is placed in it is not surrounded by <div> tags
    //sorry if that doesn't make sense
    return (
        <Fragment>                
            <form onSubmit={onSubmit}>
                <input type="file" accept=".slp" onChange={onChange}/>
                <input type="submit" value="Upload!" />
                <p>{replayData}</p>
            </form>
        </Fragment>
    )
}

export default UploadArea
