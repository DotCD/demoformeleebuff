const { default: SlippiGame } = require('@slippi/slippi-js');
//this is a list of all the characters linked to their character ID
//I later learned the slippi library has a module for these IDs along with all color alts but was too lazy to convert to it for a simple demo project tbh
const characters = {
    0: "Captain Falcon",
    1: "Donkey Kong",
    2: "Fox",
    3: "Mr. Game & Watch",
    4: "Kirby",
    5: "Bowser",
    6: "Link",
    7: "Luigi",
    8: "Mario",
    9: "Marth",
    10: "Mewtwo",
    11: "Ness",
    12: "Peach",
    13: "Pikachu",
    14: "Ice Climbers",
    15: "Jigglypuff",
    16: "Samus",
    17: "Yoshi",
    18: "Zelda",
    19: "Sheik",
    20: "Falco",
    21: "Young Link",
    22: "Dr. Mario",
    23: "Roy",
    24: "Pichu",
    25: "Ganondorf",
    26: "Master Hand",
    27: "Male Wire Frame",
    28: "Female Wire Frame",
    29: "Giga Bowser",
    30: "Crazy Hand",
    31: "Sandbag",
    32: "Popo",
    33: "None"
}

//module.exports allows us to import the functions within to other files as modules
module.exports = {
    //takes in a replay name and returns the player names
    getNames(replay) {
        try {
            //creates path from replay name
            const path = './replays/raws/' + replay
            //creates slippi game from file a path
            const game = new SlippiGame(path);
            //slippi library's function for getting the meta data of a game
            const metadata = game.getMetadata();
            //put players names into a object variable
            let names = {
                 player0: metadata.players[0].names,
                 player1: metadata.players[1].names
             }
            return names;
        } catch(err) {
            throw(err);
        }

    },
    getHeroes(replay) {
        try {
            //this basically works the same as the get names function
            const path = './replays/raws/' + replay
            const game = new SlippiGame(path);
            const settings = game.getSettings();
            const player0 = characters[settings.players[0].characterId];
            const player1 = characters[settings.players[1].characterId];
            const heroes = {
                player0: player0,
                player1: player1
            }
    
            return heroes;
        } catch(err) {
            throw(err)
        }

    },
    getMeta(replay) {
        //basically the same as the other two
        //this was a debug function
        try {
            const path = './replays/raws/' + replay
            const game = new SlippiGame(path);
            const data = game.getMetadata();
            console.log(data)
            return data;
        } catch(err) {
            throw(err)
        }
    }
};
