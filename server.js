//express library import
//express is a epic library for making backend servers super easy to dev it's very cool
const express = require('express');
//this is a library made for making handeling file uploads in express much easier
const fileUpload = require('express-fileupload');
//this is the base file system library included with node js
const fs = require('fs');
//this is the parser.js file/module
const { getNames, getHeroes, getMeta } = require('./parser');
//I don't remember why I added this
const { parse } = require('path');

//this creates a instance of the express server
const app = express();

//this makes the server use the file upload library
app.use(fileUpload());

  //okay so I don't have enough time to explain/comment how express works in detail 
  //but basically app.get is for recieving HTTP GET requests 
  //and app.post is for HTTP POST etc..
  //I will comment code that is specific to the replay parser demo

//this allows the server to take HTTP requests from the same PC it's being hosted on
//this code would usually be removed from a live production server
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });


app.get('/', function(req, res) {
    const htmlDir = __dirname + '/' + 'index.html';
    res.sendFile(htmlDir);
});

app.get('/components/:name', function(req, res) {
    const scriptDir = __dirname + '/components/' + req.params.name;
    res.sendFile(scriptDir);
});

app.get('/api/replays/:name', function(req, res) {
    const replayDir = __dirname + '/replays/' + req.params.name;
    res.sendFile(replayDir);
});

//this is the function that's called when someone makes a POST request to upload a file
app.post('/api/parse', function(req, res) {
    //if there is no file in the request
    if(req.files === null) {
        //return bad request status with message
        return res.status(400).json({ msg: 'no file uploaded'});
    }

    //file from request
    const file = req.files.file;
    //move file to directory in server
    file.mv(`${__dirname}/replays/raws/${file.name}`, err => {
        //if there is a error
        if(err) {
            console.log(err);
            return res.status(500).send(err);
        }
        //parses replay
        parseReplay(file.name);
        //send json response with file name and directory of JSON data on server
        res.json({ fileName: file.name, filePath: `/api/replays/${file.name}.json`});
    });
});

//this function calls functions from parser.js then thurns the data into a JSON file and saves it
async function parseReplay(fileName) {
    //this is location the JSON will be saved to
    const dataDest = './replays/' + fileName + '.json';  
    //funtions to get names and heroes from parser.js
    const names = getNames(fileName);
    const heroes = getHeroes(fileName);
    //the json string
    const replayData = {
        player0: {
            name: names.player0,
            hero: heroes.player0
        },
        player1: {
            name: names.player1,
            hero: heroes.player1
        }
    }
    //convert from a JS object to actual JSON (syntax between the two is 99% the same but you have to do this JIC)
    const jsonData = JSON.stringify(replayData);
    //write the JSON to a file (will replace a pre-existing file with same name)
    fs.writeFile(dataDest, jsonData, function(err, result) {
         if(err) {
             console.log('error:', err);
         }
     });
    return;
}

//sets the port variable to a enviroment variable of PORT, if that enviroment variable doesn't exist sets port to 8080
const port = process.env.PORT || 8080;
//starts server
app.listen(port, () => console.log(`App avalibe on http://localhost:${port}`));